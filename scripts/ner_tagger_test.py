import spacy
from spacy import displacy
from collections import defaultdict


class NER_Tagger:
	"""
	The aim of this class is to detect named entities in a phrase
	"""
	def __init__(self,nlp):
		self.nlp = nlp
		
	def detect_entities(self,sentence):
		"""
		The aim of this function is to analyse a given sentence and detect named entities present in the sentence
		Args: sentence[str] : a sentence to analyse
		"""
		doc = self.nlp(sentence)
		for ent in doc.ents:
			if ent.label_ != "DATE" and ent.label_ != "MONEY":
				print(ent.text, ent.label_)
		return doc

	def sort_probas(self, threshold, doc):
		"""
		The aim of this function is to sort the entities present by the confidence value given by the model
		"""

		beams = self.nlp.get_pipe('ner').beam_parse([ doc ], beam_width = 20, beam_density = 0.0001)

		entity_scores = defaultdict(float)
		for beam in beams:
			for score, ents in self.nlp.get_pipe('ner').moves.get_beam_parses(beam):
				if score >= threshold:
					for start, end, label in ents:
						
						entity_scores[(doc[start:end], label)] += score
					
		return sorted(entity_scores.items(), key=lambda x: x[1], reverse = True)

if __name__ == "__main__":
	nlp = spacy.load('en_core_web_trf')
	ner = NER_Tagger(nlp)
	sentence = input("Enter the phrase to analyze: ")
	doc = ner.detect_entities(sentence)
	print(ner.sort_probas(0.5, doc))
	displacy.render(doc, style = 'ent')



