# NER_TAGGER

The aim of this project is to create a program capable of detecting named entities in a given string.   
I present two methods to solve this problem :
- Using a ready-made Spacy NER tagger that is situated in the notebook called ```NER_spacy.ipynb``` 
- Training my own Transformer (BERT) model on conll dataset that is situated in the notebook called ```Training_NER.ipynb``` 


